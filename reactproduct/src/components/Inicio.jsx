import React, { Component } from 'react';
import { Card, Button, Image } from 'react-bootstrap';

import ImagenBienvenida from '../assets/img/foto_2.jpg';

class Inicio extends Component {

    constructor(props) {
        super(props);
        this.handleToListaCategorias.bind(this);
        this.handleToListaMateriales.bind(this);
    }
    

    handleToListaCategorias  = () => {
        this.props.history.push('/categoria');
    }
    
    handleToListaMateriales  = () => {
        this.props.history.push('/material');
    }

    render() {
        return (
            <div>
                <Card className="text-center">
                    <Card.Title>
                        <h1>Prueba modelo para Redime Cia Ltda</h1>
                    </Card.Title>
                    <Card.Body>
                    <Image width={777} height={525} src={ImagenBienvenida} alt={"ImagenBienvenida"} />
                    </Card.Body>
                    <Card.Footer>
                        <Button type="button" className="mr-4" variant="primary" onClick={this.handleToListaCategorias}>Listado de categorias</Button>
                        <Button type="button" variant="primary" onClick={this.handleToListaMateriales}>Listado de materiales</Button>
                    </Card.Footer>
                </Card>
            </div>
        );
    }
}

export default Inicio;