import React, { Component } from 'react';
import { Form, Button, Container } from 'react-bootstrap';

class AdminMaterial extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            material: {},
            categorias: [],
            nombre: '',
            descripcion: '',
            estado: '',
            stock_minimo: '',
            categoria_id: '',
        }
        
        this.handleChangeNombre = this.handleChangeNombre.bind(this);
        this.handleOnBack = this.handleOnBack.bind(this);
    }

    componentDidMount() {
        console.log(this.props.match.params);
        const { id } = this.props.match.params;

        fetch("http://127.0.0.1:8000/api/categorias/")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        categorias: result.data
                    });
                },
                (error) => {
                    this.setState({
                    error
                    });
                }
            )

        if (id !== null && id !== undefined && +id > 0) {
            this.setState({id});
            fetch("http://127.0.0.1:8000/api/materiales/" + id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        material: result.data
                    });
                    this.setState({estado:result.data.estado});
                    this.setState({nombre:result.data.nombre});
                    this.setState({descripcion:result.data.descripcion});
                    this.setState({stock_minimo:result.data.stock_minimo});
                    this.setState({categoria_id:result.data.categoria_id});
    
                },
                (error) => {
                    this.setState({
                    error
                    });
                }
            )
        }
    }
    
    handleChangeNombre = event => {
        this.setState({ nombre: event.target.value });
    }
    
    handleChangeDescripcion = event => {
        this.setState({ descripcion: event.target.value });
    }

    handleChangeStock = event => {
        this.setState({ stock_minimo: event.target.value });
    }

    handleChangeEstado = event => {
        this.setState({ estado: event.target.value });
    }

    handleChangeCategoria = event => {
        this.setState({categoria_id: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        const today = new Date();
        const formData = this.state.material;
        console.log(this.state.material);
        console.log(formData);
        if (this.state.id === 0) {
            formData.estado = this.state.estado;
            formData.nombre = this.state.nombre;
            formData.descripcion = this.state.descripcion;
            formData.categoria_id = this.state.categoria_id;
            formData.stock_minimo = this.state.stock_minimo;
            formData.creado_a = today.getFullYear() + "-"+ parseInt(today.getMonth()+1) +"-"+ today.getDate();
            fetch("http://127.0.0.1:8000/api/materiales/",{
                method: 'POST',
                body: JSON.stringify(formData),
                headers: new Headers({ 'Content-type': 'application/json' })
            }).then(response => response.json()).then(
                (result) => {
                    this.props.history.goBack();
                },
                (error) => {
                    console.log(error);
                }
            ); 
        }else {
            formData.estado = this.state.estado;
            formData.nombre = this.state.nombre;
            formData.descripcion = this.state.descripcion;
            formData.categoria_id = this.state.categoria_id;
            formData.stock_minimo = +this.state.stock_minimo;
            fetch("http://127.0.0.1:8000/api/materiales/" + this.state.id,{
                method: 'PUT',
                body: JSON.stringify(formData),
                headers: new Headers({ 'Content-type': 'application/json' })
            }).then(response => response.json()).then(
                (result) => {
                    this.props.history.goBack();
                },
                (error) => {
                    console.log(error);
                }
            ); 
        }
        
    }

    handleOnBack = () => {
        this.props.history.goBack();
    }

    render() {
        const {material,categorias} = this.state;
        return (
            <div>
                <Container className="text-left mt-5">
                    <h1>Administrar Material</h1>
                    <Form onSubmit={this.handleSubmit.bind(this)}>
                        <Form.Group controlId="nombre">
                            <Form.Label>Nombre : </Form.Label>
                            <Form.Control defaultValue={material.nombre} onChange={this.handleChangeNombre} type="text" placeholder="Ingrese nombre"  required/>
                        </Form.Group>

                        <Form.Group controlId="descripcion">
                            <Form.Label>Descripcion : </Form.Label>
                            <Form.Control defaultValue={material.descripcion} onChange={this.handleChangeDescripcion} type="text" placeholder="Ingrese descripcion"  required/>
                        </Form.Group>

                        <Form.Group controlId="stock_minimo">
                            <Form.Label>Stock minimo : </Form.Label>
                            <Form.Control defaultValue={material.stock_minimo} onChange={this.handleChangeStock} type="number" placeholder="Ingrese stock"  required/>
                        </Form.Group>

                        <Form.Group controlId="categorias">
                            <Form.Label>Categoria : </Form.Label>
                            <Form.Control as="select" value={material.categoria_id} onChange={this.handleChangeCategoria} required>
                                <option key={0} value={""}>Seleccione Categoria</option>
                            {
                                categorias.map((categoria) =>
                                    <option key={categoria.id} value={categoria.id}>{categoria.nombre}</option>
                                )
                            }
                            </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="estado">
                            <Form.Label>Estado : </Form.Label>
                            <Form.Control as="select" value={material.estado} onChange={this.handleChangeEstado} required>
                                <option key={0} value={""}>Seleccione Estado</option>
                                <option key={1} value={"ACTIVO"}>ACTIVO</option>
                                <option key={2} value={"CANCELADO"}>CANCELADO</option>
                                <option key={3} value={"ELIMINADO"}>ELIMINADO</option>
                            </Form.Control>
                        </Form.Group>

                        <div className="text-right">
                            <Button variant="primary" type="submit" className="mr-2">
                                Guardar
                            </Button>
                            <Button type="button" onClick={this.handleOnBack} variant="danger">Cancelar</Button>
                        </div>
                    </Form>
                </Container>
            </div>
        );
    }
}

export default AdminMaterial;