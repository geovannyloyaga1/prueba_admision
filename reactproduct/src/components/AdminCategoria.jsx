import React, { Component } from 'react';
import { Form, Button, Container } from 'react-bootstrap';

class AdminCategoria extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: 0,
            categoria: {},
            nombre: '',
            estado: ''
        }
        
        this.handleChangeNombre = this.handleChangeNombre.bind(this);
        this.handleOnBack = this.handleOnBack.bind(this);
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        if (id !== null && id !== undefined && +id > 0) {
            this.setState({id});
            fetch("http://127.0.0.1:8000/api/categorias/" + id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                    categoria: result.data
                    });
                    this.setState({estado:result.data.estado});
                    this.setState({nombre:result.data.nombre});
    
                },
                (error) => {
                    this.setState({
                    error
                    });
                }
            )
        }
    }
    
    handleChangeNombre = event => {
        this.setState({ nombre: event.target.value });
    }

    handleChangeEstado = event => {
        this.setState({ estado: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const today = new Date();
        const formData = this.state.categoria;
        if (this.state.id === 0) {
            formData.estado = this.state.estado;
            formData.nombre = this.state.nombre;
            formData.creado_a = today.getFullYear() + "-"+ parseInt(today.getMonth()+1) +"-"+ today.getDate();
            fetch("http://127.0.0.1:8000/api/categorias/",{
                method: 'POST',
                body: JSON.stringify(formData),
                headers: new Headers({ 'Content-type': 'application/json' })
            }).then(response => response.json()).then(
                (result) => {
                    this.props.history.goBack();
                },
                (error) => {
                    console.log(error);
                }
            ); 
        }else {
            formData.nombre = this.state.nombre;
            formData.estado = this.state.estado;
            formData.actualizado_a = today.getFullYear() + "-"+ parseInt(today.getMonth()+1) +"-"+ today.getDate();
            fetch("http://127.0.0.1:8000/api/categorias/" + this.state.id,{
                method: 'PUT',
                body: JSON.stringify(formData),
                headers: new Headers({ 'Content-type': 'application/json' })
            }).then(response => response.json()).then(
                (result) => {
                    this.props.history.goBack();
                },
                (error) => {
                    console.log(error);
                }
            ); 
        }
        
    }

    handleOnBack = () => {
        this.props.history.goBack();
    }

    render() {
        const {categoria} = this.state; 
        return (
            <div>
                <Container className="text-left mt-5">
                    <h1>Administrar Categoria</h1>
                    <Form onSubmit={this.handleSubmit.bind(this)}>
                        <Form.Group controlId="nombre">
                            <Form.Label>Nombre : </Form.Label>
                            <Form.Control defaultValue={categoria.nombre} onChange={this.handleChangeNombre} type="text" placeholder="Ingrese nombre" />
                        </Form.Group>

                        <Form.Group controlId="estado">
                            <Form.Label>Estado : </Form.Label>
                            <Form.Control as="select" value={categoria.estado} onChange={this.handleChangeEstado}>
                                <option key={1} value={"ACTIVO"}>ACTIVO</option>
                                <option key={2} value={"CANCELADO"}>CANCELADO</option>
                                <option key={3} value={"ELIMINADO"}>ELIMINADO</option>
                            </Form.Control>
                        </Form.Group>

                        <div className="text-right">
                            <Button variant="primary" type="submit" className="mr-2">
                                Guardar
                            </Button>
                            <Button type="button" onClick={this.handleOnBack} variant="danger">Cancelar</Button>
                        </div>
                    </Form>
                </Container>                
            </div>
        );
    }
}

export default AdminCategoria;