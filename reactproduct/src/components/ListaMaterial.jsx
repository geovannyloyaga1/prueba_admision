import React, { Component } from 'react';
import { Table, Container, Button } from 'react-bootstrap';


class ListaMaterial extends Component {

    
    constructor(props) {
        super(props);
        this.state = {
            materiales: [],
            error: null
        };
        this.handleOnCreate = this.handleOnCreate.bind(this);
        
    }
    
    componentDidMount() {
        fetch("http://127.0.0.1:8000/api/materiales/")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        materiales: result.data
                    });
                },
                (error) => {
                    this.setState({
                    error
                    });
                }
            )
    }

    handleOnEditmaterial = (material) => {
        const {id} = material;
        this.props.history.push("/material/admin/"+id);
    }

    handleOnDeletematerial = (material) => {
        const { id, name } = material;
        fetch("http://127.0.0.1:8000/api/materiales/" + id,{
            method: 'DELETE'
        }).then(response => response.json()).then(
            (result) => {
                alert(`La material ${result.data.name} fue eliminado de la lista.`);
                this.props.history.goBack();
            },
            (error) => {
                alert(`La material ${name} no se pudo eliminar.`);
            }
        ); 
    }

    handleOnCreate = () => {
        this.props.history.push("/material/admin/");
    }

    handleOnBack = () => {
        this.props.history.push("/");
    }
    
    render() {
        const {materiales} = this.state; 
        return (
            <div>
                <Container className="text-center  mt-5">
                    <h2>Lista de materiales</h2>
                    <div className="text-right mt-2 mb-2">
                        <Button type="button" className="float-left" onClick={this.handleOnBack} variant="secondary"> Regresar</Button>
                        <Button type="button" onClick={this.handleOnCreate} variant="success"> Nueva material</Button>
                    </div>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Estado</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Stock</th>
                                <th>Categoria</th>
                                <th>Fecha Creado</th>
                                <th>Fecha Actualizado</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                materiales.map(material => (
                                    <tr key={material.id}>
                                        <td>{material.id}</td>
                                        <td>{material.estado}</td>
                                        <td>{material.nombre}</td>
                                        <td>{material.descripcion}</td>
                                        <td>{material.stock_minimo}</td>
                                        <td>{material.categoria_id}</td>
                                        <td>{material.creado_a}</td>
                                        <td>{material.actualizado_a}</td>
                                        <td>
                                            <Button type="button" onClick={this.handleOnEditmaterial.bind(this,material)} className="mr-2">Editar</Button>
                                            <Button type="button"  onClick={this.handleOnDeletematerial.bind(this,material)} variant="danger">Eliminar</Button>
                                        </td>
                                    </tr>
                                    )
                                )                                    
                            }
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default ListaMaterial;