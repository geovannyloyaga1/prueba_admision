import React, { Component } from 'react';
import { Table, Container, Button } from 'react-bootstrap';

class ListaCategoria extends Component {

    constructor(props) {
        super(props);
        this.state = {
            categorias: [],
            error: null
        };
        this.handleOnCreate = this.handleOnCreate.bind(this);
        
    }
    
    componentDidMount() {
        fetch("http://127.0.0.1:8000/api/categorias/")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        categorias: result.data
                    });
                },
                (error) => {
                    this.setState({
                    error
                    });
                }
            )
    }

    handleOnEditCategoria = (categoria) => {
        const {id} = categoria;
        this.props.history.push("/categoria/admin/"+id);
    }

    handleOnDeleteCategoria = (categoria) => {
        const { id, name } = categoria;
        fetch("http://127.0.0.1:8000/api/categorias/" + id,{
            method: 'DELETE'
        }).then(response => response.json()).then(
            (result) => {
                alert(`La categoria ${result.data.name} fue eliminado de la lista.`);
                this.props.history.goBack();
            },
            (error) => {
                alert(`La categoria ${name} no se pudo eliminar.`);
            }
        ); 
    }

    handleOnCreate = () => {
        this.props.history.push("/categoria/admin/");
    }

    handleOnBack = () => {
        this.props.history.push("/");
    }


    render() {
        const { categorias } = this.state;
        return (
            <div>
                <Container className="text-center mt-5">
                    <h2>Lista de Categorias</h2>
                    <div className="text-right mt-2 mb-2">
                        <Button type="button" className="float-left" onClick={this.handleOnBack} variant="secondary"> Regresar</Button>
                        <Button type="button" onClick={this.handleOnCreate} variant="success"> Nueva Categoria</Button>
                    </div>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Estado</th>
                                <th>Nombre</th>
                                <th>Fecha Creado</th>
                                <th>Fecha Actualizado</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                categorias.map(categoria => (
                                    <tr key={categoria.id}>
                                        <td>{categoria.id}</td>
                                        <td>{categoria.estado}</td>
                                        <td>{categoria.nombre}</td>
                                        <td>{categoria.creado_a}</td>
                                        <td>{categoria.actualizado_a}</td>
                                        <td><Button type="button" onClick={this.handleOnEditCategoria.bind(this,categoria)} className="mr-2">Editar</Button><Button type="button"  onClick={this.handleOnDeleteCategoria.bind(this,categoria)} variant="danger">Eliminar</Button></td>
                                    </tr>
                                    )
                                )                                    
                            }
                        </tbody>
                    </Table>
                </Container>
            </div>
        );
    }
}

export default ListaCategoria;