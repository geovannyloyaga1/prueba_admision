import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import Inicio from './components/Inicio';
import ListaCategoria from './components/ListaCategoria';
import ListaMaterial from './components/ListaMaterial';
import AdminCategoria from './components/AdminCategoria';
import AdminMaterial from './components/AdminMaterial';

function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path="/" component={Inicio} />
        <Route exact path="/categoria" component={ListaCategoria} />
        <Route exact path="/material" component={ListaMaterial} />
        <Switch>
          <Route exact path="/categoria/admin/:id?" component={AdminCategoria} />
          <Route exact path="/material/admin/:id?" component={AdminMaterial} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
