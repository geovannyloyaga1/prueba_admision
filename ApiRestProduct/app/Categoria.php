<?php

namespace App;

use App\Materiale;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    public $timestamps = false;
    //
    protected $fillable = [
        'estado',
        'nombre',
        'creado_a',
        'actualizado_a',
    ];

    public function material()
    {
        return $this->hasMany(Materiale::class);
    }
}
