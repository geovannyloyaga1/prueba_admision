<?php

namespace App\Http\Controllers\Categoria;

use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DateTime;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categorias = Categoria::all();

        return response()->json([ 'data' => $categorias ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $campos = $request->all();

        $categoria = Categoria::create($campos);

        return response()->json([ 'data' => $categoria],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $categoria = Categoria::findOrFail($id);

        return response()->json(['data' => $categoria], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $categoria = Categoria::findOrFail($id);
        if ($request->has('estado')) {
            $categoria->estado = $request->estado;
        }
        if ($request->has('nombre')) {
            $categoria->nombre = $request->nombre;
        }
        if (!$categoria->isDirty()) {
            return response()->json(['error'=>'Se debe especificar al menos un valor diferente para actualizar'],422);
        }
        $categoria->actualizado_a = new DateTime();
        $categoria->save();
        return response()->json([ 'data' => $categoria ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $categoria = Categoria::findOrFail($id);
        $categoria->delete();

        return response()->json([ 'data' => $categoria ], 200);
    }
}
