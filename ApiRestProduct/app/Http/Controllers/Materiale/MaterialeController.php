<?php

namespace App\Http\Controllers\Materiale;

use App\Materiale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DateTime;

class MaterialeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $materiales = Materiale::all();

        return response()->json([ 'data' => $materiales ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos = $request->all();

        $material = Materiale::create($campos);

        return response()->json([ 'data' => $material],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $material = Materiale::findOrFail($id);

        return response()->json(['data' => $material], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $material = Materiale::findOrFail($id);
        if ($request->has('estado')) {
            $material->estado = $request->estado;
        }
        if ($request->has('nombre')) {
            $material->nombre = $request->nombre;
        }
        if ($request->has('descripcion')) {
            $material->descripcion = $request->descripcion;
        }
        if ($request->has('stock_minimo')){
            $material->stock_minimo = $request->stock_minimo;
        }
        if ($request->has('categoria_id')){
            $material->categoria_id = $request->categoria_id;
        }
        if (!$material->isDirty()) {
            return response()->json(['error'=>'Se debe especificar al menos un valor diferente para actualizar'],422);
        }
        $material->actualizado_a = new DateTime();
        $material->save();
        return response()->json([ 'data' => $material ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $material = Materiale::findOrFail($id);
        $material->delete();

        return response()->json([ 'data' => $material ], 200);
    }
}
