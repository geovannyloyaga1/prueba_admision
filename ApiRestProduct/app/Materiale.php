<?php

namespace App;

use App\Categoria;
use Illuminate\Database\Eloquent\Model;

class Materiale extends Model
{
    public $timestamps = false;
    //
    protected $fillable = [
        'estado',
        'nombre',
        'descripcion',
        'stock_minimo',
        'categoria_id',
        'creado_a',
        'actualizado_a',
    ];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }
}
